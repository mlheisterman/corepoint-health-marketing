<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

function corepointhealth_webform_mail_headers($variables) {
  $headers = array(
    'X-Mailer' => 'Drupal Webform (PHP/' . phpversion() . ')',
	'Content-Type' => 'application/x-gm-impdata',
	// 'To' => '{$GM-WEBIMPORT$}<ap@neotool.com>'
	// 'Cc' => 'mlheisterman@gmail.com',
	// 'From' => 'SURVEY Integration Engine<ap@corepointhealth.com>'
  );
  // // Add BCC on registrant email.
  //   if ($variables['email']['eid'] == '1') {
  //     $headers['cc'] = 'mlheisterman@mindspring.com';
  //     $headers['Content-Type'] = 'application/x-gm-impdata';
  //     $headers['From'] = 'SURVEY Integration Engine<ap@corepointhealth.com>';
  //     $headers['To'] = 'ap@neotool.com';
  //   }
  // kpr($variables); // die();
  return $headers;
}

/**
 * Add a css class to the body based on the taxonomy values applied to a page.
 */
function taxonomy_node_get_terms($node, $key = 'tid') {
    if(arg(0)=='node' && is_numeric(arg(1))) {  
        static $terms;
        if (!isset($terms[$node->vid][$key])) {
            $query = db_select('taxonomy_index', 'r');
            $t_alias = $query->join('taxonomy_term_data', 't', 'r.tid = t.tid');
            $v_alias = $query->join('taxonomy_vocabulary', 'v', 't.vid = v.vid');
            $query->fields( $t_alias );
            $query->condition("r.nid", $node->nid);
            $result = $query->execute();
            $terms[$node->vid][$key] = array();
            foreach ($result as $term) {
                $terms[$node->vid][$key][$term->$key] = $term;
            }
        }
        return $terms[$node->vid][$key];
    }
}

 function corepoint_preprocess_html(&$variables) {
     $node = node_load(arg(1));
     $results = taxonomy_node_get_terms($node);
     if(is_array($results)) {
         foreach ($results as $item) {
            $variables['classes_array'][] = "taxonomy-".strtolower(drupal_clean_css_identifier($item->name)); 
         }
     }
 }

 function corepoint_compare_taxonomy($nid, $tid) {
 	$node = node_load($nid);
 	if (isset($node->taxonomy_catalog[$node->language][0]['tid'])) {
 		if ($tid == $node->taxonomy_catalog[$node->language][0]['tid']) {
 	 		return 'active';
 		}
 		return '';
 	}
 }
//function corepointhealth_uc_cart_checkout_form_form_alter(&$form, &$form_state) {
//  $form['panes']['comments']['#title'] = t('Additional Comments or Needs');
//}

function corepointhealth_preprocess_html(&$variables) {
  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
$element = array(
  '#type' => 'html_tag',
  '#tag' => 'meta',
  '#attributes' => array('http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1'),
  '#weight' => -1001,
);
drupal_add_html_head($element, 'chrome_frame');
}

