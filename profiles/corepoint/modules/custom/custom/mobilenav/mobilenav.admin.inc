<?php
// $Id$

/**
 * Admin page callback for the mobile nav switcher settings form.
 * @file
 */
function mobilenav_system_settings_form($form, $automatic_defaults = TRUE) {
  $form['actions']['#type'] = 'container';
  $form['actions']['#attributes']['class'][] = 'form-actions';
  $form['actions']['#weight'] = 100;
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  if ($automatic_defaults) {
    $form = _system_settings_form_automatic_defaults($form);
  }

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'][] = 'system_settings_form_submit';
  // By default, render the form using theme_system_settings_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }
  return $form;
}

/**
 * Module settings form.
 */
function mobilenav_admin_settings() {
  $blocksList = mobilenav_get_blocks_list();

  $form['mobilenav_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Pixel Width at which the Mobile Nav Switcher Activates (Defaults to 480)'),
    '#description' => t('This setting will determine the width at which this module takes over.'),
    '#default_value' => variable_get('mobilenav_width', 480),
    '#required' => TRUE,
  );
  $form['mobilenav_touch'] = array(
    '#type' => 'checkbox',
    '#title' => t('Switch to mobile nav block if device is touch capable.'),
    '#description' => t('Check here if you want the switcher to change to the mobile nav block when viewed on a mobile device.'),
    '#default_value' => variable_get('mobilenav_touch'),
    '#required' => FALSE,
  );
  $form['mobilenav_mobile'] = array(
    '#type' => 'select',
    '#title' => t('Mobile First Block'),
    '#description' => t('This is the block that will appear by default (for mobile devices).'),
    '#default_value' => variable_get('mobilenav_mobile'),
    '#options' => $blocksList,
    '#required' => TRUE,
  );
    $form['mobilenav_wide'] = array(
    '#type' => 'select',
    '#title' => t('Wide Screen'),
    '#description' => t('This is the block that will appear if the screen width for the display is wider than set above.'),
    '#default_value' => variable_get('mobilenav_wide'),
    '#options' => $blocksList,
    '#required' => TRUE,
  );
  return mobilenav_system_settings_form($form, FALSE);
}

/**
 * Calls core function that is used to build the blocks list on /admin/structure/block 
 * @return type 
 */

function mobilenav_get_blocks_list() {
  $blocksList = _block_rehash();
  
  foreach ($blocksList as $block) {
  //$blocks[$block['module']][$block['delta']] = $block['info']; //THIS IS THE ARRAY IF YOU WANT TO RETURN OPTION GROUPS GROUPED BY MODULE
  $blocks[$block['module'] . ' - ' . $block['delta']] = $block['info']; //THIS IS FOR RETURNING THE FLATTENED VERSION
  }
  return $blocks;
}