; Include Build Kit install profile makefile via URL
; Brochureware Plus Site Build Kit
includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org.make



; ====== MODULE EXCLUDES ==================================================
projects[openid] = FALSE
projects[openidadmin] = FALSE



; ====== UI TOOLS =========================================================
; Admin
projects[admin][subdir] = contrib
projects[admin][version] = 2.0-beta3

; Wysiwyg
projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = 2.1

; CKEditor
projects[ckeditor][subdir] = contrib
projects[ckeditor][version] = 1.6
libraries[ckeditor_lib][download][type] = "file"
libraries[ckeditor_lib][directory_name] = "ckeditor"
libraries[ckeditor_lib][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"

; CKFinder
libraries[ckfinder_lib][download][type] = "file"
libraries[ckfinder_lib][directory_name] = "ckfinder"
libraries[ckfinder_lib][download][url] = "http://download.cksource.com/CKFinder/CKFinder%20for%20PHP/2.1.1/ckfinder_php_2.1.1.tar.gz"



; ====== DEVELOPMENT TOOLS ================================================
; Devel
projects[devel][subdir] = contrib
projects[devel][version] = 1.2

; Diff
projects[diff][subdir] = contrib
projects[diff][version] = 2.0

; CTools
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.0-rc1

; Features
projects[features][subdir] = contrib
projects[features][version] = 1.0-beta6

; Libraries API
projects[libraries][subdir] = contrib
projects[libraries][version] = 2.0-alpha2

; profile2
; Using dev version of profile 2 just after 1.1 to fix testing namespace issue (1342064).
projects[profile2][subdir] = contrib
projects[profile2][download][type] = file
projects[profile2][download][url] = "http://drupalcode.org/project/profile2.git/snapshot/323da230068178236bde48637e0a3f10eb91cd78.tar.gz"

; Omega Tools
projects[omega_tools][subdir] = contrib
projects[omega_tools][version] = 3.0-rc4

; Strongarm
projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0-beta5


; Apache Solr Search Integration
projects[apachesolr][subdir] = contrib
projects[apachesolr][version] = 1.0-beta16

; Entity API
projects[entity][subdir] = contrib
projects[entity][version] = 1.0-rc1

; Rules
projects[rules][subdir] = contrib
projects[rules][version] = 2.0

; Ubercart
projects[ubercart][subdir] = contrib
projects[ubercart][version] = 3.0

; ====== LIBRARIES ================================================

; Profiler library (helper utilities for install profiles)
libraries[profiler][download][type] = file
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.0-beta1.tar.gz"

; Colorbox
projects[colorbox][subdir] = contrib
projects[colorbox][version] = 1.2
libraries[colorbox_lib][download][type] = file
libraries[colorbox_lib][directory_name] = colorbox
libraries[colorbox_lib][download][url] = "https://github.com/jackmoore/colorbox/zipball/master"


; ====== SITE BUILDING TOOLS ================================================
; Context
projects[context][subdir] = contrib
projects[context][version] = 3.0-beta2

; Views
projects[views][subdir] = contrib
projects[views][version] = 3.3

; Views Accordion
projects[views_accordion][subdir] = contrib
projects[views_accordion][version] = 1.0-rc1

; Views Bulk Operations
projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = 3.0-rc1


; Date
projects[date][subdir] = contrib
projects[date][version] = 2.2


; Superfish
projects[superfish][subdir] = contrib
projects[superfish][version] = 1.8
libraries[superfish_lib][download][type] = file
libraries[superfish_lib][directory_name] = superfish
libraries[superfish_lib][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal/zipball/master"

; Quick Tabs
projects[quicktabs][subdir] = contrib
projects[quicktabs][version] = 3.3

; Panels
projects[panels][subdir] = contrib
projects[panels][version] = 3.0

; FileField Sources
projects[filefield_sources][subdir] = contrib
projects[filefield_sources][version] = 1.4

; Delta
projects[delta][subdir] = contrib
projects[delta][version] = 3.0-beta9

; Token
projects[token][subdir] = contrib
projects[token][version] = 1.0-rc1

; Facet API
projects[facetapi][subdir] = contrib
projects[facetapi][version] = 1.0-rc4

; Organic Groups
projects[og][subdir] = contrib
projects[og][version] = 1.3

; G2 Glossary 
; (BROKEY - WON'T ALLOW BUILD)
; Error: syntax error, unexpected T_STRING, expecting ']' in
; /Applications/MAMP/htdocs/corepoint/builds/corepoint_master/profiles/corepoint/modules/contrib/g2/g2.install, line 32
; projects[g2][subdir] = contrib
; projects[g2][version] = 1.x-dev



; ====== THEMES ==============================================================
; Tao
projects[tao][subdir] = contrib
;projects[tao][version] = 3.0-beta4

; Rubik
projects[rubik][subdir] = contrib
;projects[rubik][version] = 4.0-beta8

; Omega Base
projects[omega][subdir] = contrib
;projects[omega][version] = 3.1