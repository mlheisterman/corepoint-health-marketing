<?php

/**
 * @file
 * Payment pack administration menu items.
 *
 */

/**
 * Receive a Purchase Order for an order.
 */
function uc_purchase_order_receive_purchase_order_form($form, $form_state, $order) {
  $balance = uc_payment_balance($order);
  $form['balance'] = array('#markup' => uc_currency_format($balance));
  $form['order_id'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => uc_currency_format($balance, FALSE, FALSE, '.'),
    '#size' => 10,
    '#field_prefix' => variable_get('uc_sign_after_amount', FALSE) ? '' : variable_get('uc_currency_sign', '$'),
    '#field_suffix' => variable_get('uc_sign_after_amount', FALSE) ? variable_get('uc_currency_sign', '$') : '',
  );
  $form['comment'] = array(
    '#type' => 'textfield',
    '#title' => t('Purchase Order No. / Comment'),
    '#description' => t('Any notes about the purchase order, like receipt number.'),
    '#size' => 64,
    '#maxlength' => 256,
  );
  $form['clear'] = array(
    '#type' => 'fieldset',
    '#title' => t('Expected payment date'),
    '#collapsible' => FALSE,
  );
  $form['clear']['clear_month'] = uc_select_month(NULL, format_date(REQUEST_TIME, 'custom', 'n'));
  $form['clear']['clear_day'] = uc_select_day(NULL, format_date(REQUEST_TIME, 'custom', 'j'));
  $form['clear']['clear_year'] = uc_select_year(NULL, format_date(REQUEST_TIME, 'custom', 'Y'), format_date(REQUEST_TIME, 'custom', 'Y'), format_date(REQUEST_TIME, 'custom', 'Y') + 1);
  foreach (array('clear_month', 'clear_day', 'clear_year') as $key) {
    $form['clear'][$key]['#prefix'] = '<div style="float: left; margin-right: 1em;">';
    $form['clear'][$key]['#suffix'] = '</div>';
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Receive Purchase Order'),
  );

  return $form;
}

/**
 * Generate form to receive a purchase order
 */
function theme_uc_purchase_order_receive_purchase_order_form($variables) {
  $form = $variables['form'];
  drupal_add_js(drupal_get_path('module', 'uc_payment') . '/uc_payment.js');

  $output = '<p>' . t('Use the form to enter the purchase order into the payments system and set the expected receipt date.') . '</p>';
  $output .= '<p><strong>' . t('Order balance:') . '</strong> '
           . drupal_render($form['balance']) . '</p>';

  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Validate purchase order reception form
 */
function uc_purchase_order_receive_purchase_order_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['amount'])) {
    form_set_error('amount', t('The amount must be a number.'));
  }
}

/**
 * Submit purchase order reception form
 */
function uc_purchase_order_receive_purchase_order_form_submit($form, &$form_state) {
  global $user;

  uc_payment_enter($form_state['values']['order_id'], 'purchase_order',
                  $form_state['values']['amount'], $user->uid, '', $form_state['values']['comment']);

  db_insert('uc_payment_purchase_order')
    ->fields(array(
      'order_id' => $form_state['values']['order_id'],
      'clear_date' => mktime(12, 0, 0, $form_state['values']['clear_month'], $form_state['values']['clear_day'], $form_state['values']['clear_year']),
    ))
    ->execute();

  drupal_set_message(t('Purchase order received, expected payment date of @date.', array(
    '@date' => date(variable_get('uc_date_format_default', 'm/d/Y'), mktime(12, 0, 0, $form_state['values']['clear_month'], $form_state['values']['clear_day'], $form_state['values']['clear_year'])),
  )));

  drupal_goto('admin/store/orders/' . $form_state['values']['order_id']);
}
