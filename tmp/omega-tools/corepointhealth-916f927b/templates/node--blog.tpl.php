<article<?php print $attributes; ?>>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "dccc5947-86e7-4895-907d-3dc9932ee256"}); </script>
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted"><abbr title="<?php print format_date($node->created, 'custom', "l, F j, Y - H:i"); ?>">
	 <?php print format_date($node->created, 'long_no_time'); ?></abbr>
  <?php
	$author_account = user_load($node->uid);
	if (!empty($author_account)) {
	  print '<em>';
	  print t('by ');
	  print $author_account->field_first_name['und'][0]['value'];
	  print ' ';
	  print $author_account->field_last_name['und'][0]['value'];
	  print '</em>';
	} ?>
  </footer>
  <br>
  <br>
  <?php endif; ?>  
  
  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>
  
  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
<span class='st_sharethis_hcount' displayText='ShareThis'></span>
<span class='st_fblike_hcount' displayText='Facebook Like'></span>
<span class='st_linkedin_hcount' displayText='LinkedIn'></span>
<span class='st_googleplus_hcount' displayText='Google +'></span>
<span class='st_twitter_hcount' displayText='Tweet'></span>
<span class='st_email_hcount' displayText='Email'></span>
</article>
