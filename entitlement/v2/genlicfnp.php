<?php

include('../../../community.corepointhealth.com/profiles/drupal_commons/themes/contrib/commons_origins/custom/lic-settings.php');


/*******************************************
*	genlic.php
*	by VM Foundry
*
*	THIS PAGE IS NOT USED BY A STANDARD BROWSER
*
*	Corepoint products each contain
*	wizards for obtaining a license for
*	use. Those wizards connect to this page
*	to obtain their licenses.
*
*	This page verifies user login and
*	entitlement id and attempts to cut
*	an appropriate license.
*
*	Created: 03 February 2009
*	By: msmith
*
*	HISTORY
*	
*	3/17/2009: MJS Customized the response based on product name and whether or not it was a demo key. Hardcoded new product names into the response.     
*	3/20/2009: M Smith changed $SQLverifyProductKey query to look for the new "product_key_disabled" flag. this new feature allows us to disable individual keys.
*
	6/14/2010: Ginger Minton - copied from genlic.php and modified for RLM
*******************************************/





/**************************************
***       GRAB THE POSTED INFO      ***
**************************************/

// get input from raw post
$INPUT = array();
$data = $GLOBALS["HTTP_RAW_POST_DATA"];

$data_rows = split( "\n", $data );
if ( sizeof( $data_rows ) ) {
    foreach ( $data_rows as $row ) {
        if ( $row ) {
            $pair = explode( "=", $row, 2 );
            $INPUT[$pair[0]] = trim( $pair[1] );
        }
    }
}

// set defaults for the values
$hostname = "UNKNOWN";
$hostid	= "UNKNOWN";
$machine_description = "UNKNOWN";
$number_cpus = "0";
$os	= "UNKNOWN";
$product_name = "";
$product_version = "2.0";
$release = "";
$password = "";
$username = "";
$windows_user = "";
$licenseContent = "";

$thisDate = date("Y-m-d"); // today's date. we need it in a few places

// if values exist in the post, assign them to variables (overwriting our defaults)
if ( isset( $INPUT["cn"]  )) { $hostname		= $INPUT["cn"];		}
if ( isset( $INPUT["hid"] )) { $hostid			= $INPUT["hid"];	}
if ( isset( $INPUT["md"]  )) { $machine_description	= $INPUT["md"];		}
if ( isset( $INPUT["np"]  )) { $number_cpus		= $INPUT["np"];		}
if ( isset( $INPUT["os"]  )) { $os  			= $INPUT["os"];		}
if ( isset( $INPUT["pn"]  )) { $product_name		= $INPUT["pn"];		}
if ( isset( $INPUT["pv"]  )) { $product_version		= $INPUT["pv"];		}
if ( isset( $INPUT["rl"]  )) { $release                 = $INPUT["rl"];
}
if ( isset( $INPUT["pwd"] )) { $password		= $INPUT["pwd"];	}
if ( isset( $INPUT["uid"] )) { $username		= $INPUT["uid"];	}
if ( isset( $INPUT["wu"]  )) { $windows_user            = $INPUT["wu"];
}
if ( isset( $INPUT["eid"] )) { $eid			= $INPUT["eid"];	}

// print the defaults
/*print($hostid."\n");
print($hostname."\n");
print($machine_description."\n");
print($number_cpus."\n");
print($os."\n");
print($password."\n");
print($product_name."\n");
print($product_version."\n");
print($username."\n");
print($licenseContent."\n");
exit;*/



	// set default for the EID, if it doesn't exist.
	if(!$eid) {


	if($product_name == "CIE") {
		$eid = "1bcf641648f8102cb65c000c29caf37c"; // DEFAULT NEOINTEGRATE KEY (Demo Key)
		$isDemoKey = true;
	} else {
		$eid = "c6a4a6fa49aa102cb65c000c29caf37c"; // DEFAULT NEOBROWSE KEY (Demo Key)
		$isDemoKey = true;
	}

		// set a flag that this is a demo license. we'll need to know this later on
		$isDemo = true;
	}

	$isNeoBrowse = false;


	// strip any dashes from the eid. then put them back in
	// this is the only way to be sure the key is in the correct format
	// no matter how it's passed in
	$eid = trim($eid);
	$eid = rtrim($eid);
	$eid = str_replace("-", "", $eid);

	$eid1 = substr($eid, 0, 8);
	$eid2 = substr($eid, 8, 4);
	$eid3 = substr($eid, 12, 4);
	$eid4 = substr($eid, 16, 4);
	$eid5 = substr($eid, 20, 31);
	$eid = "$eid1-$eid2-$eid3-$eid4-$eid5";








// strip slashes in the host ID
$hostid = ereg_replace( "\"", "", $hostid );

// Drupal passwords are md5 hashed. So we'll need to covert the submitted raw password to a hash
// to compare it to the one in the DB
$password = md5($password);


// see if they have supplied all the required parameters
if ( ! $username
  || ! $password ) {
    print( "<ERROR>You must supply a login and password.</ERROR>" );
    exit;
}

if ( ! $product_name
  || ! $product_version
  || ! $hostid ) {
   print( "<ERROR>You must supply a product, product version and host id value.</ERROR>" );
   exit;
}

//Strip special characters from description field (the ' character was causing issues with MySql)  Mike Stockemer added June 18, 2009
$machine_description = preg_replace('/[^a-z0-9\s]/i','',$machine_description);

/**************************************
***   CHECK THE USER'S LOGIN INFO   ***
**************************************/

$SQLauthUsername = "SELECT * FROM users WHERE name = '$username'";
$resultAuthUserName = mysql_query($SQLauthUsername);
$numUsernames = mysql_num_rows($resultAuthUserName);

if($numUsernames > 0) {

//////VMF-k Adds///////////////////////////////////////
$jump_mail_add = mysql_fetch_array($resultAuthUserName);
$mail_gm_pass = $jump_mail_add[mail];
//////////////////////////////////////////////////////
	// separate checks for password & associated company
	$SQLauthPassword = "SELECT u.uid, u.name, u.mail
	FROM users u
	WHERE u.name = '$username'
	AND u.pass = '$password'";
	$resultAuthPassword = mysql_query($SQLauthPassword);
	$numPasswords = mysql_num_rows($resultAuthPassword);

	if ($numPasswords > 0) { // The credentials are good
		// now check for company ID
		$SQLauthCompany = "SELECT u.uid, u.name, u.mail, c.cid
		FROM users u, users_company c
		WHERE u.name = '$username'
		AND u.pass = '$password'
		AND u.uid = c.uid";
		$resultAuthCompany = mysql_query($SQLauthCompany);
		$numCompanies = mysql_num_rows($resultAuthCompany);
		
		if ($numCompanies == 1) { // The credentials check out, and there is only one company ID associated with this user
			while($arr_result = mysql_fetch_array($resultAuthCompany)) {
				$accountno = $arr_result[cid]; // user's company ID
				$userId = $arr_result[uid]; // user's ID
			}
		} elseif($numCompanies < 1) { // The user is not associated with any company
			print("<ERROR>Your account ($username) is not authorized to generate the type of license you requested. Please contact support@corepointhealth.com to request a license or for further information.</ERROR>");
			exit;
		} else { // There is more than one company ID associated with this user
			print("<ERROR>Your account ($username) has been mistakenly attached to multiple companies. Please contact support@corepointhealth.com to correct this error.</ERROR>");
			exit;
		}

	} else { // The credentials failed
		print("<ERROR>The login / password you supplied does not match our records. Please go back and try again.</ERROR>");
		exit;
	}

} else { // username does not exist

	print("<ERROR>The login you supplied ($username) does not exist.\n\nIf you have not already created a Corepoint Health web account, please go to http://community.corepointhealth.com and click the 'login / register' link at the top of the page.</ERROR>");
	exit;

}





/**************************************
***   CHECK THE ENTITLEMENT INFO    ***
**************************************/
if ($eid) {

	if(!$isDemoKey) { // if this isn't a demo key

	$SQLverifyProductKey = "SELECT product_key_id, product_id, product_key_available, product_key_string_id, product_key_expiration FROM ProductKeys_ProductKey WHERE product_key_string_id = '$eid' AND product_key_disabled = 0 AND company_id = $accountno AND (product_key_expiration >= '$thisDate' OR ISNULL(product_key_expiration))";
	
	} else { // if this IS a demo key, don't bother checking against the company ID
	
	$SQLverifyProductKey = "SELECT product_key_id, product_id, product_key_available, product_key_string_id, product_key_expiration FROM ProductKeys_ProductKey WHERE product_key_string_id = '$eid'";
	
	}
	
	$resultVerifyProductKey = mysql_query($SQLverifyProductKey);
	$numKeys = mysql_num_rows($resultVerifyProductKey);

	if($numKeys > 0) { // valid key found

		while($arr_keys = mysql_fetch_array($resultVerifyProductKey)) {
			if($arr_keys[product_key_available] < 1) {
				print("<ERROR>You have requested the maximum number of licenses for this product key. Please contact support@corepointhealth.com if you wish to have more keys added.</ERROR>");
				exit;
			} else {
				$productKeyId = $arr_keys[product_key_id];
				$productKey = $arr_keys[product_id];

				$productStringKey = $arr_keys[product_key_string_id];

				if($isDemo) {

					//it's a demo--expire it 30 days from now
					$timeStamp = strtotime($thisDate);
					$timeStamp += 24 * 60 * 60 * 30; // (add 30 days)
					$productKeyExpiration = date("j-M-Y", $timeStamp);

				} else {
					$productKeyExpiration = $arr_keys[product_key_expiration]; //the key's actual expiration
				}

				// format the expiration
				if(!$productKeyExpiration) {
					$productKeyExpiration = "permanent";
				} else {
					$productKeyExpiration = date("j-M-Y", strtotime($productKeyExpiration));
				}

			}
		}

	} else { // no valid key found

		print("<ERROR>The product key you supplied is not valid.  If this problem persists, please contact support@corepointhealth.com</ERROR>");
		exit;

	}


	/**************************************
	***      GET THE PRODUCT INFO       ***
	**************************************/
	// Based on the product key returned in the last query

	$SQLgetProductInfo = "SELECT * FROM ProductKeys_Product WHERE product_id = $productKey";
	$resultGetProductInfo = mysql_query($SQLgetProductInfo);
	$numProducts = mysql_num_rows($resultGetProductInfo);

	if($numProducts > 0) { // found the base product info. set some variables

		while($arr_product = mysql_fetch_array($resultGetProductInfo)) {
			$prName = $arr_product[prod_flexlm_name];
			$prCompany = $arr_product[prod_company];
			$prGmName= $arr_product[prod_goldmine_name];
			$prGmVersion= $arr_product[prod_version];

			// Corepoint rebranding
			if ($prName == "NeoIntegrate") {
				$prName = "CIE";
			}
			
			if ($prCompany == "NEOTOOL") {
				$prCompany = "corepnt";
			}
		}

		$SQLgetProductFeatures = "SELECT f.feature_name, kf.feature_value
		FROM ProductKeys_Feature f, ProductKeys_ProductKeyFeatures kf, ProductKeys_ProductKey pk
		WHERE pk.product_key_string_id ='$productStringKey'
		AND pk.product_key_id = kf.product_key_id
		AND kf.feature_id = f.feature_id";

		$resultGetProductFeatures = mysql_query($SQLgetProductFeatures);
		$numProductFeatures = mysql_num_rows($resultGetProductFeatures);

		if($numProductFeatures > 0) {

			while($arr_features = mysql_fetch_array($resultGetProductFeatures)) {

				// put the feature attributes into variables
				$featureName = $arr_features[feature_name];
				$featureValue = $arr_features[feature_value];

				if(!$featureValue) { // check for empty feature values
					$featureValue = "";
				} else {
					$featureValue = "VENDOR_STRING=".$featureValue;
				}

				// write the feature line!
				/*$licenseContent .= "LICENSE $prCompany ".$prName."-$featureName 2.0 $productKeyExpiration uncounted hostid=\"$hostid\" $featureValue\r\n";*/
				$licenseContent .= "FEATURE ".$prName."-$featureName $prCompany 2.0 $productKeyExpiration uncounted $featureValue HOSTID=\"$hostid\" TS_OK SIGN=0\r\n";
			}

		} else {

			print("<ERROR>The selected product has no features available.</ERROR>");
			exit;

		}

	} else {
		print("<ERROR>No product found with this product ID. Please contact support@corepointhealth.com to correct this issue.</ERROR>");
		exit;
	}


} // end if($eid)

////////////////////GOLDMINE////////////////////////BEGIN
////////////////////////////////////////////////////
// GOLDMINE WEB IMPORT
$summarized_notes_mk = preg_replace("/[\n|\r]/",'',$licenseContent);
$request_info = "Win user = $windows_user; OS = $os; prod = $product_name; prod ver = $product_version; rel = $release; lic exp = $productKeyExpiration; # cpus = $number_cpus; host = $hostname; host id = $hostid;";
$time_event = date("g:i:s a");
$msg = "
[Instructions]
PASSWORD=foo@bar1
DupCheck=Email

[ContSupp]
CS1_RECTYPE=P
CS1_CONTACT=LIC
CS1_CONTSUPREF=$prGmName $prGmVersion $time_event
CS1_Notes=$request_info 

[Data]
EMAIL=$mail_gm_pass

";

$Email = "license@corepointhealth.com";
$subject = "Web Import";
$header = "Return-Path: $Email\r\n";
$header .= "From: license<$Email>\r\n";
$header .= "To: {\$GM-WEBIMPORT\$}<ap@neotool.com>\r\n";
$header .= "Content-Type: application/x-gm-impdata;\r\n";
$toEmail = "{\$GM-WEBIMPORT\$}<ap@neotool.com>";
$mail_sent = @mail($toEmail, $subject, "$msg", "$header");
//////////////////////////////////////////////////////
////////////////////GOLDMINE////////////////////////END



/**************************************************
***   FORMAT THE INFO INTO A LICENSE REQUEST    ***
**************************************************/
$RequestXML = '<WebRequest type="LicenseEncrypt">
  <ContactDetails>
    <username>'.$username.'</username>
    <e_mail></e_mail>
  </ContactDetails>
  <License>'.$licenseContent.'</License>
</WebRequest>';

/**************************************************
***   SEND THE LICENSE REQUEST TO THE SERVICE   ***
**************************************************/
include '../common.php'; // tcp request and xml parsing utilities
$xmlDoc = new DOMDoc;
$objPost = new ServerXMLHTTP;
$postLoc = "udp://72.48.93.13:9998"; // Host CRM
$objPost->open( "POST", $postLoc );
$objPost->send($RequestXML);
$xmlReceive = $objPost->responseXML;
$returned_object = $xmlReceive->xml; // The response

/*********************************************************
***   FORMAT THE RESPONSE FOR RETURN TO THE SOFTWARE   ***
*********************************************************/

// this is a license for Corepoint Health Integration engine
if ($isDemo){ //create string for demo key with expiration dates
    $formattedResponse = "<BODY>Thank you for your interest in Corepoint Health!<br><br>You now have an evaluation license for the Corepoint Integration Engine that will expire on $productKeyExpiration.<br><br>If you would like to purchase the Corepoint Integration Engine, please contact sales@corepointhealth.com.<br><br>For support-related questions, please contact support@corepointhealth.com.</BODY>";
}
else if ($productKeyExpiration == "permanent"){ //permanent production license
    $formattedResponse = "<BODY>Congratulations!<br><br>You now have a permanent license for your Corepoint Integration Engine installation.<br><br>For support-related questions, please contact support@corepointhealth.com.</BODY>";
}
else{ //dev or backup license with some expiration date
   $formattedResponse = "<BODY>Congratulations!<br><br>You now have a license for your Corepoint Integration Engine installation that will expire on $productKeyExpiration.<br><br>For support related questions, please contact support@corepointhealth.com.</BODY>"; 
}  

$formattedResponse .= "<LICENSE>$returned_object</LICENSE>";

// strip any line breaks out, then add them back in. we were having some issues with
// unusual line breaks when viewed through the software, and this fixed it.
$formattedResponse = preg_replace("/[\n|\r]/",'',$formattedResponse);
$formattedResponse = str_replace("&#8216;","'", $formattedResponse);
$formattedResponse = str_replace("&#8217;","'", $formattedResponse);
$formattedResponse = preg_replace('/[^(\x20-\x7F)]*/','', $formattedResponse);
$formattedResponse = str_replace("FEATURE", "\nFEATURE", $formattedResponse);

//add line feeds back into the description text be replacing the <br> references
$formattedResponse = str_replace("<br>","\n",$formattedResponse);

print $formattedResponse; // print it!



/*********************************************************
*** LOG THIS KEY, AND ADJUST THE USER'S AVAILABLE KEYS ***
*********************************************************/
$cleanedLicense = str_replace('"', '\"', $returned_object); // prepare the license for DB logging

	$SQLlogReturnedKey = "INSERT INTO ProductKeys_ProductKeyHistory(product_key_hist_time,
	product_key_hist_hostid,
	product_key_hist_hostname,
	product_key_hist_machine_description,
	product_key_hist_license,
	product_key_hist_OS,
	product_key_id,
	user_id,
	product_key_hist_win_user_id,
	product_key_hist_product_release,
	product_key_hist_host_num_cpus)
	VALUES('$thisDate',
	'$hostid',
	'$hostname',
	'$machine_description',
	'$cleanedLicense',
	'$os',
	$productKeyId,
	$userId,
	'$windows_user',
	'$release',
	$number_cpus)";
	$runLogger = mysql_query($SQLlogReturnedKey);
    
	if(!$isDemo) { // if it's NOT a demo license, remove one from the available licenses
		$SQLremoveAvailableLicense = "UPDATE ProductKeys_ProductKey SET product_key_available = (product_key_available - 1) WHERE product_key_id = $productKeyId";
		$runLicenseChange = mysql_query($SQLremoveAvailableLicense);
	}

?>
