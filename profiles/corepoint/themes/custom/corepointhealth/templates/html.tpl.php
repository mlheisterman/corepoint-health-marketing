<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>  
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript">
	var _gsc = _gsc || [];
	_gsc.push(['_setWebsite', '50GWQ4JB']);
	(function() {
		var gsc = document.createElement('script'); gsc.type = 'text/javascript'; gsc.async = true;
		gsc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'c.getsmartcontent.com/gsc.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gsc, s);
	})();
  </script>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body<?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
