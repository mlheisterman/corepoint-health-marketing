<?php

/* @file
 * Provides the Purchase Order payment method.
 */

/**
 * Implements hook_init().
 */
function uc_purchase_order_init() {
  global $conf;
  $conf['i18n_variables'][] = 'uc_purchase_order_method_title';
  $conf['i18n_variables'][] = 'uc_purchase_order_manager_name';
  $conf['i18n_variables'][] = 'uc_purchase_order_manager_title';
  $conf['i18n_variables'][] = 'uc_purchase_order_manager_email';
  $conf['i18n_variables'][] = 'uc_purchase_order_manager_phone';
  $conf['i18n_variables'][] = 'uc_purchase_order_policy';

}

/**
 * Implements hook_menu().
 */
function uc_purchase_order_menu() {
  $items['admin/store/orders/%uc_order/receive_purchase_order'] = array(
    'title' => 'Receive Purchase Order',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_purchase_order_receive_purchase_order_form', 3),
    'access arguments' => array('view all orders'),
    'type' => MENU_CALLBACK,
    'file' => 'uc_purchase_order.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function uc_purchase_order_theme() {
  return array(
    'uc_purchase_order_receive_purchase_order_form' => array(
      'render element' => 'form',
      'file' => 'uc_purchase_order.admin.inc',
    ),
    'uc_purchase_order_information' => array(
      'variables' => array('oid' => NULL),
    ),
  );
}

/**
 * Implements hook_uc_payment_method().
 */
function uc_purchase_order_uc_payment_method() {
  $methods[] = array(
    'id' => 'purchase_order',
    'name' => t('Purchase Order'),
    'title' => variable_get('uc_purchase_order_method_title', 'Purchase Order'),
    'desc' => t('Pay by Purchase Order.'),
    'callback' => 'uc_payment_method_purchase_order',
    'weight' => 1,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );
  return $methods;
}

/**
 * Implements hook_token_info().
 */
function uc_purchase_order_token_info_alter(&$data) {
  $data['tokens']['uc_order']['payment-po-details'] = array(
    'name' => t('Payment Purchase Order Details'),
    'description' => t('The purchase order details of the order.'),
  );
}

/**
 * Implements hook_tokens().
 */
function uc_purchase_order_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'uc_order' && !empty($data['uc_order'])) {
    $order = $data['uc_order'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'payment-po-details':
          $replacements[$original] = theme('uc_purchase_order_information', array('oid' => $order->order_id));
          break;
      }
    }
  }

  return $replacements;
}

/**
 * Implements hook_uc_invoice_templates().
 */
function uc_purchase_order_uc_invoice_templates() {
  return array('customer-purchase_order');
}
/**
 * Handle the Purchase Order payment method.
 */
function uc_payment_method_purchase_order($op, &$order, $form = NULL, &$form_state = NULL) {
  switch ($op) {
    case 'cart-details':
      $details = uc_payment_method_purchase_order_form(array(), $form_state, $order);
      return $details;
	case 'cart-process':
	  if (!isset($form_state['values']['panes']['payment']['details']['po_number'])) {
	  return;
	  }
	  // Fetch the CC details from the $_POST directly.
	  $po_data = $form_state['values']['panes']['payment']['details'];

      $po_data['po_number'] = str_replace(' ', '', $po_data['po_number']);

      array_walk($po_data, 'check_plain');

      // Go ahead and put the CC data in the payment details array.
      $order->payment_details = $po_data;

      // Default our value for validation.
      $return = TRUE;

      // Make sure a manager name value was entered.
      if (empty($po_data['po_manager_name'])) {
	    form_set_error('panes][payment][details][po_manager_name', t('Enter the name of an approved manager.'));
        $return = FALSE;
      }

      // Validate the PO number if that's turned on/check for non-digits.
      if (empty($po_data['po_number'])) {
        form_set_error('panes][payment][details][po_number', t('Please enter a purchase order number.'));
        $return = FALSE;
      }

      // Make sure a manager title value was entered.
      if (empty($po_data['po_manager_title'])) {
	    form_set_error('panes][payment][details][po_manager_title', t('Enter the title of the approving manager.'));
        $return = FALSE;
      }
      // Make sure a manager email value was entered.
      if (empty($po_data['po_manager_email'])) {
	    form_set_error('panes][payment][details][po_manager_email', t('Enter the email of the approving manager.'));
        $return = FALSE;
      }
    
  	  // Make sure a manager name value was entered.
      if (empty($po_data['po_manager_phone'])) {
	    form_set_error('panes][payment][details][po_manager_phone', t('Enter the phone number of the approving manager.'));
        $return = FALSE;
      }

	  return $return;
	
    case 'cart-review':
      $review[] = array('title' => t('Purchase Order Number'), 'data' => check_plain($order->payment_details['po_number']));
      $review[] = array('title' => t('Approving Manager - Name'), 'data' => check_plain($order->payment_details['po_manager_name']));
      $review[] = array('title' => t('Approving Manager - Title'), 'data' => check_plain($order->payment_details['po_manager_title']));
      $review[] = array('title' => t('Approving Manager - Email'), 'data' => check_plain($order->payment_details['po_manager_email']));
      $review[] = array('title' => t('Approving Manager - Phone'), 'data' => check_plain($order->payment_details['po_manager_phone']));
      return $review;

    case 'order-view':
      if (!variable_get('uc_payment_tracking', TRUE)) {
        return '';
      }
      $result = db_query("SELECT payment_date FROM {uc_payment_purchase_order} WHERE order_id = :id ", array(':id' => $order->order_id));
      if ($clear_date = $result->fetchField()) {
        $output = t('Payment Date:') . ' ' . format_date($clear_date, 'custom', variable_get('uc_date_format_default', 'd/m/Y'));
      }
      else {
        $output = l(t('Receive Purchase Order'), 'admin/store/orders/' . $order->order_id . '/receive_purchase_order');
      }
      $output .= '<br />';
      return array('#markup' => $output);

    case 'customer-view':
      if (!variable_get('uc_payment_tracking', TRUE)) {
        return '';
      }
      $result = db_query("SELECT payment_date FROM {uc_payment_purchase_order} WHERE order_id = :id ", array(':id' => $order->order_id));
      $output = t('Status: pending');
      if ($clear_date = $result->fetchField()) {
        $output = t('Purchase order received') . '<br />'
                . t('Expected payment date:') . '<br />' . format_date($clear_date, 'custom', variable_get('uc_date_format_default', 'd/m/Y'));
      }
      return array('#markup' => $output);

    case 'settings':
      // help text
      $form['uc_purchase_order_help_text'] = array(
        '#markup' => '<div class="help">' . t('<h4><strong>Installation instructions</strong></h4><p>For better customer experience please use the token [order-payment-po-details] to display the p.o. details on the invoice. You can find an example invoice template doing this in the uc_purchase_order module folder.</p>') . '</div>',
      );
      // settings
      $form['uc_purchase_order_method_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Payment method title'),
        '#description' => t('Provide specific description for the payment method on the order checkout page.'),
        '#default_value' => variable_get('uc_purchase_order_method_title', 'Purchase order'),
      );
      $form['uc_purchase_order_policy'] = array(
        '#type' => 'textarea',
        '#title' => t('Payment instructions'),
        '#description' => t('Instructions for customers on the checkout page. Use &lt;br /&gt; for line break.'),
        '#default_value' => variable_get('uc_purchase_order_policy', ''),
        '#rows' => 3,
      );
      return $form;
  }
}

/**
 * Displays the credit card details form on the checkout screen.
 */
function uc_payment_method_purchase_order_form($form, &$form_state, $order) {

  // But we have to accommodate failed checkout form validation here.

  if (!isset($order->payment_details) && isset($form_state['values']['panes']['payment']['details'])) {
    $order->payment_details = $form_state['values']['panes']['payment']['details'];
    $order->payment_details['po_number'] = str_replace(' ', '', $order->payment_details['po_number']);
  }

  if (!isset($order->payment_details)) {
    $order->payment_details = array();
  }
  $form['po_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Purchase order number'),
    '#attributes' => array('autocomplete' => 'off'),
    '#size' => 60,
    '#maxlength' => 60,
  );
  $form['po_manager_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Approving Manager - Name'),
    '#default_value' => isset($order->payment_details['po_manager_name']) ? $order->payment_details['po_manager_name'] : '',
    '#attributes' => array('autocomplete' => 'off'),
    '#size' => 32,
    '#maxlength' => 64,
  );
  $form['po_manager_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Approving Manager - Title'),
    '#default_value' => isset($order->payment_details['po_manager_title']) ? $order->payment_details['po_manager_title'] : '',
    '#attributes' => array('autocomplete' => 'off'),
    '#size' => 40,
    '#maxlength' => 40,
  );
  $form['po_manager_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Approving Manager - Email'),
    '#default_value' => isset($order->payment_details['po_manager_email']) ? $order->payment_details['po_manager_email'] : '',
    '#attributes' => array('autocomplete' => 'off'),
    '#size' => 64,
    '#maxlength' => 64,
  );
  $form['po_manager_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Approving Manager - Phone'),
    '#default_value' => isset($order->payment_details['po_manager_phone']) ? $order->payment_details['po_manager_phone'] : '',
    '#attributes' => array('autocomplete' => 'off'),
    '#size' => 14,
    '#maxlength' => 14,
  );
  return $form;
}

function _uc_credit_save_po_data_to_order($cc_data, $order_id) {
  // Load up the existing data array.
  $data = db_query("SELECT data FROM {uc_orders} WHERE order_id = :id", array(':id' => $order_id))->fetchField();
  $data = unserialize($data);

  // Save it again.
  db_update('uc_orders')
    ->fields(array('data' => serialize($data)))
    ->condition('order_id', $order_id)
    ->execute();
}
/**
 * Theme output displayed in checkout review, etc.
 */
// function theme_uc_bank_transfer_bank_details($variables) {
//   $oid = $variables['oid'];
//   $output = uc_bank_transfer_bank_details($oid);
//   return $output;
// }

/**
 * Implementation of uc_bank_transfer_bank_details($oid).
 *
 * $oid = order ID
 */
// function uc_bank_transfer_bank_details($oid) {
//   if (variable_get('uc_bank_transfer_account_owner', '') <> '') {
//     $bank_info[] = t('Account owner') . ': ' . variable_get('uc_bank_transfer_account_owner', '');
//   }
//   if (variable_get('uc_bank_transfer_account_number', '') <> '') {
//     $bank_info[] = t('Account number') . ': ' . variable_get('uc_bank_transfer_account_number', '');
//   }
//   if (variable_get('uc_bank_transfer_account_iban', '') <> '') {
//     $bank_info[] = t('IBAN') . ': ' . variable_get('uc_bank_transfer_account_iban', '');
//   }
//   if (variable_get('uc_bank_transfer_bank_code', '') <> '') {
//     $bank_info[] = variable_get('uc_bank_transfer_bank_code_appellation', 'Bank code') . ': ' . variable_get('uc_bank_transfer_bank_code', '');
//   }
//   if (variable_get('uc_bank_transfer_account_swift', '') <> '') {
//     $bank_info[] = t('SWIFT') . ': ' . variable_get('uc_bank_transfer_account_swift', '');
//   }
//   if (variable_get('uc_bank_transfer_account_bank', '') <> '') {
//     $bank_info[] = t('Banking institution') . ': ' . variable_get('uc_bank_transfer_account_bank', '');
//   }
//   if (variable_get('uc_bank_transfer_account_branch', '') <> '') {
//     $bank_info[] = t('Branch office') . ': ' . variable_get('uc_bank_transfer_account_branch', '');
//   }
//   if (variable_get('uc_bank_transfer_subject_oid', FALSE)) {
//     isset($bank_info) ? ($bank_info[] = '<br />' . t('Reason for payment') . ': ' . t('order number') . ' ' . $oid) : ($bank_info[] = t('Reason for payment') . ': ' . t('order number') . ' ' . $oid);
//   }
//   if (variable_get('uc_bank_transfer_policy', '') <> '') {
//     isset($bank_info) ? ($bank_info[] = '<br />' . variable_get('uc_bank_transfer_policy', '')) : ($bank_info[] = variable_get('uc_bank_transfer_policy', ''));
//   }
// 
//   isset($bank_info) ? ($bank_details = implode('<br />', $bank_info)) : ($bank_details = '');
// 
//   return $bank_details;
// }

