<?php

//--------------------------------------------------------------------------
// this is a work-around because at least one host provider
// header("Location: ...") // does not work.
//--------------------------------------------------------------------------
function Redirect($url)
{
  //header("HTTP/1.1 302 Found");
  //header("Location: $url");
  //header("Connection: close");

  echo <<< END_ECHO
<HTML><HEAD><META http-equiv="refresh" content="0; url=$url"></HEAD></HTML>
END_ECHO;
}

//--------------------------------------------------------------------------
// version of html_entities_decode() for pre-4.3.0 PHP servers
//--------------------------------------------------------------------------
if ( !function_exists( "html_entities_decode" ) )
{
  function html_entities_decode($string)
  {
       $string = strtr($string, array_flip(get_html_translation_table(HTML_ENTITIES)));
       $string = preg_replace("/&#([0-9]+);/me", "chr('\\1')", $string);
       return $string;
  }
}

//--------------------------------------------------------------------------
function text_dump($data)
{
  echo "<BR><TEXTAREA COLS=80 ROWS=8>".$data."</TEXTAREA><BR>\n";
}

//------------------------------------------------------
// returns an associate array with name & value
//------------------------------------------------------
function ParseQueryString( $qs )
{
  $avps = explode("&", $qs);
  foreach ($avps as $avp)
  {
    list($name, $value) = explode("=", $avp);
    $name = urldecode($name);
    $value = urldecode($value);

    $values[$name] = $value;
  }

  return $values;
}

//--------------------------------------------------------------------------
function LogMessage($msg, $type="info")
{

  $parts = explode(".", $_SERVER["HTTP_HOST"]);
  $domain = $parts[count($parts)-2].".".$parts[count($parts)-1];
  $referer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "";

  $logpath = dirname($_SERVER["PATH_TRANSLATED"])."";

  if ($type == "simpleorder") {
     error_log(date("m/d/Y h:i:s A") . " $msg\n",
          3, "$logpath$type.log");

  } else {
     error_log(date("m/d/Y h:i:s A")." ".
          $_SERVER["REMOTE_ADDR"].
          " $domain ".
          $_SERVER["REQUEST_URI"].
          " $type \"$msg\" \"".
          $_SERVER["HTTP_USER_AGENT"]."\" ".
          "\"$referer\"\n",
          3, "$logpath$type.log");
  }
}

//--------------------------------------------------------------------------
// Posts XML data and returns XML data
//--------------------------------------------------------------------------
function fetchXML($url, $content, $headers=null)
{
//  text_dump($content);

  $response = '';
  $url_parts = parse_url($url);

  if ( empty($url_parts['port']) ) {
     $url_parts['port'] = 9997;
  }

  $header  = "POST $url_parts[path] HTTP/1.0\r\n";
  $header .= "Host: $url_parts[host]\r\n";

  if ( !empty($headers) )
  {
    foreach( $headers as $name => $value )
    {
      $header .= "$name: $value\r\n";
    }
  }

  // if content-type not set, default to "text/xml"
  if ( !stristr($header, 'Content-type') )
    $header .= "Content-type: text/xml\r\n";

    $header .= 'Content-length: ' . strlen($content) . "\r\n\r\n";

  $fp = fsockopen ($url_parts['host'], $url_parts['port'], $errno, $errstr);
  if ( !$fp )
  {
    //LogMessage ("Connection Error: $errstr [$errno]", 'error');
    echo "<BR>Connection Error: $errstr [$errno]<BR>";
    return '';
  }

  $header="";

  fputs ($fp, $header . $content );

  $line = "";
  while (!feof($fp)) {
     // $line .= stream_get_line($fp, 10000000000, "\n");
	 //$line .= stream_get_line($fp, 10000000000, "\n");
	 $line .= stream_get_line($fp, 100000000, "\n");
  }



  //$line = fgetss($fp, 124096);
  //text_dump($line);

  list($response_protocol, $response_code, $response_string) = split(' ', $line);

  if ( $response_code != 200 )
  {
    //LogMessage ("HTTP Error: ($line)", 'error');
    $matsmith =  "$line";
   //print $matsmith;
    fclose($fp);
    return $matsmith;
  }

  do {
    $line = fgets($fp, 124096);

    if ( strstr($line, ':') ) {
      list($name, $value) = explode (':', $line);
      $response_header[$name] = trim($value);
    } elseif ( trim($line) == '' ) {
      // a blank line marks the end of the HTTP header
      break;
    }
  } while( !feof($fp) );

  for ( $response = ''; !feof($fp); $response .= fread($fp, 100000) );

  fclose($fp);

  text_dump($response);
  return $response;
}

//--------------------------------------------------------------------------
//
// e-Rx / Inetbackoffice specific functions
//
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// Import additional products offered by affiliates from "products.csv"
//
//   id must be "offsite"
//   itemNum is the URL to buy from
//   itemName is the full product name
//   itemPrice can be set to a valid price or zero
//   pillName is the short product name
//--------------------------------------------------------------------------
function ImportProducts(&$node, $productMatch, $categoryMatch)
{
  $xmlDoc = new DOMDoc;
  $nE = $xmlDoc->createElement("item");
  $nE->setAttribute("id", "offsite");

  if ( !file_exists("products.csv") )
    return;

  $lines = file("products.csv");

  foreach ($lines as $line)
  {
    $line = rtrim($line, "\r\n");
    if ( empty($line) )
      next;

    list(
      $category,
      $product,
      $url,
      $description,
      $price
    ) = explode( ",", $line);

    if ( empty($productMatch) || stristr($product, $productMatch) )
    {
      if ( empty($categoryMatch) || strcasecmp($category, $categoryMatch) === 0 )
      {
        $nE->setAttribute("pillName",  $product);
        $nE->setAttribute("itemNum",   $url);
        $nE->setAttribute("itemName",  $description);
        $nE->setAttribute("itemPrice", $price);
        $node->appendChild($nE);
      }
    }
  }
}

//----------------------------------------------------------------------------
function ItemName2Shortname($itemName)
{
  preg_match("/^[^ ^(^\/]+ ?[A-Z-]*/i", $itemName, $matches);
  $shortname = trim($matches[0]);
  return $shortname;
}

//----------------------------------------------------------------------------
function NodeCmpByItemName($node1, $node2)
{
  return strcasecmp($node1->getAttribute("itemName"), $node2->getAttribute("itemName"));
}

//--------------------------------------------------------------------------
//
// ASP replacement classes and functions
//
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class Request
{
  function ServerVariables($key)
  {
    return $_SERVER[$key];
  }

  function Form($key)
  {
    if ( !empty($key) )
      return $_POST[$key];
    else
      return $_POST;
  }

  function QueryString($key)
  {
    return $_GET[$key];
  }

  // this works for retrieving values, but not for setting them
  function Cookies()
  {
    if ( func_num_args() == 0 ) {
      $result = "";
      $i = 1;
      foreach ( $_COOKIE as $name => $value ) {
        $result .= "$name=$value";
        if ( $i++ < count($_COOKIE) )
          $result .= ";";
      }
      return $result;
    }

    if ( func_num_args() == 1 ) {
      if ( empty($_COOKIE[func_get_arg(0)]) )
        return "";

      return $_COOKIE[func_get_arg(0)];
    }

    // else func_num_args() == 2

    if ( empty($_COOKIE[func_get_arg(0)]) )
      return "";

    $cookie = ParseQueryString($_COOKIE[func_get_arg(0)]);

    if ( empty($cookie[func_get_arg(1)]) )
      return "";

    return $cookie[func_get_arg(1)];
  }

}

$request = new Request; // mimics some of the ASP request object;

//--------------------------------------------------------------------------
class Cookie
{
  var $name;
  var $values;  // associative array
  var $expires;
  var $path;
  var $domain;
  var $value;  // full string representation of values[]
}

//--------------------------------------------------------------------------
class Response
{
  var $_cookies;  // array of Cookie objects

  function End()
  {
    exit();
  }

  function Redirect($url)
  {
    header("Location: $url");
  }

  function Write($string)
  {
    echo $string;
  }

  function contentType($type)
  {
    // was a property in ASP, but has to be a method in PHP
    header("Content-type: $type");
  }

  // TODO save cookies in Microsoft format: e.g.
  //   AdTrack=binaryID=&affiliateID=&campaignGroupId=Yahoo&campaignId=rxvalue.us;
  //   response.cookies("AdTrack")("campaignGroupId") =campaignGroupId
  //   response.cookies("AdTrack")("campaignId") = campaignId
  //   response.cookies("AdTrack").expires = dateAdd("yyyy", 1, now())
  //
  // will need to persist data from each call and set $_COOKIE after
  // each call using the accumulated data.
  function Cookies($key1, $key2)
  {
    if ( func_num_args() == 1 )
    {
      $value = "";
    }

    if ( func_num_args() == 2 )
    {
      $value = "";
    }

    $_COOKIE[$key] = $value;
  }

  function CookiesExpire($cookie, $expire)
  {
  }

  function CookiesPath($path, $expire)
  {
  }

  function CookiesDomain($domain, $expire)
  {
  }
}

$response = new Response; // global

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class XMLContainer
{
  var $xml;

  function XMLContainer($xml)
  {
    $this->xml = $xml;
  }
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class IXMLDOMNode
{
  var $nodeName;
  var $text;
  var $attributes;
  var $length = 0;
  var $childNodes;
  var $parentNode;

  function IXMLDOMNode($tagName=NULL)
  {
    $this->length = 0;
    $this->nodeName = $tagName;
  }

  function setAttribute($name, $value)
  {
    $this->attributes[$name] = $value;
  }

  function getAttribute($name)
  {
    return $this->attributes[$name];
  }

  function &appendChild($node)
  {
    $this->length++;
    $node->parentNode =& $this;
    $this->childNodes[] = $node;
    return $this->childNodes[count($this->childNodes)-1];
  }

  function insertBefore($newChild, $refChild=NULL)
  {
    // TODO: implement insert before refChild
    return $this->appendChild($newChild);
  }

  function removeChild($childNode)
  {
    // TODO: implement removeChild
    return($childNode);
  }

  function xml()
  {
    return XMLNodeToText($this);
  }

  function _dump()
  {
    text_dump(XMLNodeToText($this));
  }
}

//--------------------------------------------------------------------------
// Server.CreateObject("MSXML2.DOMDocument")
// http://msdn.microsoft.com/library/en-us/xmlsdk30/htm/xmobjxmldomdocument.asp
//--------------------------------------------------------------------------
class DOMDoc extends IXMLDOMNode
{
  var $documentElement;
  var $_xml;

  function DOMDoc()
  {
    $this->documentElement =& $this;
  }

  function xml()
  {
    // TODO: not sure what to do with the main node, maybe <XMLDocument 3.0> ???
    // in the mean time, we'll send this
    $this->_xml = XMLNodeToText($this->childNodes[0]);
    return $this->_xml;
  }

  function selectNodes($name)
  {
    $result = NULL;

    if ( empty($this->childNodes[0]->childNodes) )
      return NULL;

    foreach ($this->childNodes[0]->childNodes as $node)
    {
      if ( $node->nodeName = $name ) {
        $result[] = $node;
      }
    }
    return $result;
  }

  function loadXML($xml)
  {
    $this->rawxml = $xml;

    $this->childNodes = NULL;
    $this->length     = 0;
    $this->attributes = NULL;
    $this->text       = NULL;
    $this->nodeName   = NULL;
    $this->parentNode = NULL;

    $parentNode =& $this;

    $tag_count = preg_match_all("/\<(\/)?([^>^ ^\/]*) *([^>]*)>([^<]*)?/i", $xml, $matches);

    //" comment here...

    $i=0;
    $regWhole      = $matches[$i++];
    $regCloseTag   = $matches[$i++];
    $regName       = $matches[$i++];
    $regAttributes = $matches[$i++];
    $regText       = $matches[$i++];

    for ( $i=0; $i < $tag_count; $i++)
    {
      if ( $regName[$i] == "?xml" )
        continue;

      if ( $regCloseTag[$i] == "/" )
      {
        $parentNode =& $parentNode->parentNode;
      }
      else
      {
        $node = new IXMLDOMNode($regName[$i]);
        if ( !empty($regText[$i]) )
        {
          $node->text = html_entities_decode($regText[$i]);
        }
        $node->nodeName = $regName[$i];

        $attrs = trim($regAttributes[$i]);
        if ( !empty($attrs) )
        {
          $attr_count = preg_match_all("/([^=]*)=\"([^\"]*)\"/i",  $attrs, $attr_matches);

          for ($j=0; $j < $attr_count; $j++)
          {
            $node->setAttribute(trim($attr_matches[1][$j]), html_entities_decode(trim($attr_matches[2][$j])));
          }
        }

        $child =& $parentNode->appendChild($node);

        if ( empty($attrs) || (substr($attrs, strlen($attrs)-1, 1) != "/") )
        {
          $parentNode =& $child;
        }
      }
    }
    $this->documentElement =& $this->childNodes[0];

    return TRUE;  // success
  }

  function createElement($tagName)
  {
    return new IXMLDOMNode($tagName);
  }

//  function &appendChild($node)
//  {
//    $this->length++;
//    $node->parentNode = $this;
//    $this->childNodes[] = $node;
//    $this->documentElement =& $this->childNodes[0];
//    return $this->childNodes[count($this->childNodes)-1];
//  }
}

//--------------------------------------------------------------------------
// Server.CreateObject("MSXML2.ServerXMLHTTP")
//--------------------------------------------------------------------------
class ServerXMLHTTP
{
  var $responseXML;
  var $responseText;

  var $_headers;
  var $_request;
  var $_method;
  var $_url;


  function ServerXMLHTTP()
  {
    $this->_headers['Content-type'] = 'text/xml';
  }

  function open($method, $url)
  {
    $this->_method = $method;
    $this->_url = $url;
    return;
  }

  function send($request)
  {
    $this->_request = $request;
    $this->responseText = fetchXML($this->_url, $this->_request, $this->_headers);
    $this->responseXML = new XMLContainer($this->responseText);
    return;
  }

  function setRequestHeader($name, $value)
  {
    // first make sure we remove any existing headers
    // even if case if different
    foreach ($this->_headers as $n => $v)
    {
      if ( stristr($n, $name) )
      {
        unset($this->_headers[$n]);
      }
    }

    $this->_headers[$name] = $value;
  }
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
function XMLNodeToText($node)
{
  $xml = "<".$node->nodeName;    //"

  if ( !empty($node->attributes) )
  {
    foreach ( $node->attributes as $name => $value )
    {
      $xml .= " $name=\"".htmlspecialchars($value)."\"";
    }
  }
  if ( !empty($node->text) ) {
    $xml .= ">".htmlspecialchars($node->text);
  } elseif ( empty($node->childNodes) ) {
    $xml .= "/>";
  } else {
    $xml .= ">";
  }
  if ( !empty($node->childNodes) )
  {
    foreach ($node->childNodes as $childNode) {
      $xml .= XMLNodeToText($childNode);
    }
  }
  if ( !empty($node->text) || !empty($node->childNodes) ) {
    $xml .= "</".$node->nodeName.">";
  }

  return $xml;
}

//--------------------------------------------------------------------------
Class Error
{
  var $Description;
}

$Err = NULL;

//--------------------------------------------------------------------------
Class MailSender
{
  var $Host;
  var $Subject;
  var $From;
  var $FromName;
  var $Body;
  var $Priority;
  var $_to;
  var $_bcc;
  var $_cc;

  function AddAddress( $address ) { $this->_to[]  = $address; }
  function AddCC( $address )      { $this->_cc[]  = $address; }
  function AddBCC( $address )     { $this->_bcc[] = $address; }

  function Send()
  {
    $header  = "From: $this->FromName <$this->From>\r \n";
    $header .= "Date: " . date("r") . "\r \n";

    if ( isset($this->_cc) )     { $header .= "Cc: $this->_cc[0]\r \n"; }
    if ( isset($this->_bcc) )     { $header .= "Bcc: $this->_bcc[0]\r \n"; }
    if ( isset($this->priority) ) { $header .= "X-Priority: $this->priority\r \n"; }

    $to = "";
    $i = 1;
    foreach ( $this->_to as $address ) {
      $to .= $address;
      if ( $i++ < count($this->_to) )
        $to .= ",";
    }

    $result = mail ( $to, $this->Subject, $this->Body, $header );

    if ( ! $result )
    {
      $Err = new Error;
      $Err->Description = "Mail Error.";
    } else {
      $Err = NULL;
    }
  }
}

//--------------------------------------------------------------------------
// ASP functions
//--------------------------------------------------------------------------

function Replace($subject, $search, $replace) { return str_replace($search, $replace, $subject); }
function UCase($string)                       { return strtoupper($string); }
function FormatNumber($number, $decimals)     { return number_format($number, $decimals); }
function Right($string, $start)       { return substr($string, strlen($string)-$start); }
function Left($string, $len)          { return substr($string, 0, $len); }
function Mid($string, $start, $len=0) { return substr($string, $start, $len); }
function isNumeric($var)              { return is_numeric($var); }
function Len($var)  { return strlen($var); }
function CStr($var) { return strval($var); }
function CInt($var) { return intval($var); }
function CDbl($var) { return $var; }
function Int($var)  { return intval($var); }
function Fix($var)  { return intval($var); }

//--------------------------------------------------------------------------
function MonthName($month_number)
{
  $month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  return($month_names[$month_number-1]);
}

//--------------------------------------------------------------------------
// should use PHP's money_format(), but it did not work
//--------------------------------------------------------------------------
function FormatCurrency($number)
{
// return money_format('%.2n', $number); }
  return "$".number_format($number, 2);
}

//--------------------------------------------------------------------------
// InStr([start], haystack, needle, [compare: 0=binary|1=text])
//
// start starts at 1
//--------------------------------------------------------------------------
function InStr($v1, $v2, $v3=-1, $v4=-1)
{
  if ( $v3 == -1 ) // two arguments
  {
    $offset   = 1;
    $haystack = $v1;
    $needle   = $v2;
  }
  else
  {
    $offset   = $v1;
    $haystack = $v2;
    $needle   = $v3;
  }
  return strpos($haystack, $needle, $offset-1);
}

//--------------------------------------------------------------------------
// from http://us4.php.net/manual/en/function.strrpos.php
//
function InStrRev($n,$s)
{
  $x = strpos(chr(0).strrev($n),$s)+0;
  return (($x==0) ? 0 : strlen($n)-$x+1);
}

//--------------------------------------------------------------------------
define ('vbCrLf', "\n");

?>
