<?php

/**
 * Implements HOOK_install_tasks()
 *
 * NOTE: This function must exist before profiler_v2('profile_name')
 * is called!
 */
function corepoint_install_tasks($install_state) {
  include_once('libraries/profiler/profiler_api.inc');

  return array(
    // Run profiler install tasks
    'profiler_install_profile_complete' => array(),

    // Now run custom install tasks
	
    // 'demo_accounts' => array(
    //    'display_name' => st('Install Demo Accounts'),
    //    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    //    'function' => 'schaaf_create_demo_accounts',
    //  ),
     'input_formats' => array(
     	'display_name' => st('Install Text Formats'),
		'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
		'function' => 'corepoint_install_text_formats',
     ),
	'content_types' => array(
		'display_name' => st('Install Basic Content Types (Page & Article)'),
		'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
		'function' => 'corepoint_install_page_and_article',
	),
	'image_styles' => array(
		'display_name' => st('Install Image Styles and Fields'),
		'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
		'function' => 'corepoint_install_image_fields',
	),
	'user_roles' => array(
		'display_name' => st('Install User Roles'),
		'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
		'function' => 'corepoint_install_user_roles',
	),
   );
}

/**
 * Load profiler
 */
!function_exists('profiler_v2') ? require_once('libraries/profiler/profiler.inc') : FALSE;
profiler_v2('corepoint');

function corepoint_install_text_formats() {
  // Add text formats.
	  $filtered_html_format = array(
	    'format' => 'filtered_html',
	    'name' => 'Filtered HTML',
	    'weight' => 0,
	    'filters' => array(
	      // URL filter.
	      'filter_url' => array(
	        'weight' => 0,
	        'status' => 1,
	      ),
	      // HTML filter.
	      'filter_html' => array(
	        'weight' => 1,
	        'status' => 1,
	      ),
	      // Line break filter.
	      'filter_autop' => array(
	        'weight' => 2,
	        'status' => 1,
	      ),
	      // HTML corrector filter.
	      'filter_htmlcorrector' => array(
	        'weight' => 10,
	        'status' => 1,
	      ),
	    ),
	  );
	  $filtered_html_format = (object) $filtered_html_format;
	  filter_format_save($filtered_html_format);

	  $full_html_format = array(
	    'format' => 'full_html',
	    'name' => 'Full HTML',
	    'weight' => 1,
	    'filters' => array(
	      // URL filter.
	      'filter_url' => array(
	        'weight' => 0,
	        'status' => 1,
	      ),
	      // Line break filter.
	      'filter_autop' => array(
	        'weight' => 1,
	        'status' => 1,
	      ),
	      // HTML corrector filter.
	      'filter_htmlcorrector' => array(
	        'weight' => 10,
	        'status' => 1,
	      ),
	    ),
	  );
	  $full_html_format = (object) $full_html_format;
	  filter_format_save($full_html_format);
}

function corepoint_install_page_and_article() {
  // Insert default pre-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Basic page'),
      'base' => 'node_content',
      'description' => st("Use <em>basic pages</em> for your static content, such as an 'About us' page."),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
    array(
      'type' => 'article',
      'name' => st('Article'),
      'base' => 'node_content',
      'description' => st('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
  );

  foreach ($types as $type) {
    $type = node_type_set_defaults($type);
    node_type_save($type);
    node_add_body_field($type);
  }

  // Insert default pre-defined RDF mapping into the database.
  $rdf_mappings = array(
    array(
      'type' => 'node',
      'bundle' => 'page',
      'mapping' => array(
        'rdftype' => array('foaf:Document'),
      ),
    ),
    array(
      'type' => 'node',
      'bundle' => 'article',
      'mapping' => array(
        'field_image' => array(
          'predicates' => array('og:image', 'rdfs:seeAlso'),
          'type' => 'rel',
        ),
        'field_tags' => array(
          'predicates' => array('dc:subject'),
          'type' => 'rel',
        ),
      ),
    ),
  );
  foreach ($rdf_mappings as $rdf_mapping) {
    rdf_mapping_save($rdf_mapping);
  }

  // Default "Basic page" to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_HIDDEN);

  // Don't display date and author information for "Basic page" nodes by default.
  variable_set('node_submitted_page', FALSE);
}

function corepoint_install_image_fields() {
  // Create an image field named "Image", enabled for the 'article' content type.
	  // Many of the following values will be defaulted, they're included here as an illustrative examples.
	  // See http://api.drupal.org/api/function/field_create_field/7

	  $field = array(
	    'field_name' => 'field_image',
	    'type' => 'image',
	    'cardinality' => 1,
	    'translatable' => TRUE,
	    'locked' => FALSE,
	    'indexes' => array('fid' => array('fid')),
	    'settings' => array(
	      'uri_scheme' => 'public',
	      'default_image' => FALSE,
	    ),
	    'storage' => array(
	      'type' => 'field_sql_storage',
	      'settings' => array(),
	    ),
	  );
	  field_create_field($field);


	  // Many of the following values will be defaulted, they're included here as an illustrative examples.
	  // See http://api.drupal.org/api/function/field_create_instance/7
	  $instance = array(
	    'field_name' => 'field_image',
	    'entity_type' => 'node',
	    'label' => 'Image',
	    'bundle' => 'article',
	    'description' => st('Upload an image to go with this article.'),
	    'required' => FALSE,

	    'settings' => array(
	      'file_directory' => 'field/image',
	      'file_extensions' => 'png gif jpg jpeg',
	      'max_filesize' => '',
	      'max_resolution' => '',
	      'min_resolution' => '',
	      'alt_field' => TRUE,
	      'title_field' => '',
	    ),

	    'widget' => array(
	      'type' => 'image_image',
	      'settings' => array(
	        'progress_indicator' => 'throbber',
	        'preview_image_style' => 'thumbnail',
	      ),
	      'weight' => -1,
	    ),

	    'display' => array(
	      'default' => array(
	        'label' => 'hidden',
	        'type' => 'image',
	        'settings' => array('image_style' => 'large', 'image_link' => ''),
	        'weight' => -1,
	      ),
	      'teaser' => array(
	        'label' => 'hidden',
	        'type' => 'image',
	        'settings' => array('image_style' => 'medium', 'image_link' => 'content'),
	        'weight' => -1,
	      ),
	    ),
	  );
	  field_create_instance($instance);
    
    $large_style = 'a:3:{s:5:"width";s:3:"270";s:6:"height";s:0:"";s:7:"upscale";i:0;}';
    $query = "INSERT INTO
                {image_styles}
              SET
                name='large'";
    db_query($query);
    
    $query = "INSERT INTO
                {image_effects} (isid, weight, name, data)
              VALUES (1, 0, 'image_scale', :style)";
    
    db_query($query, array(':style' => $large_style));
}

function corepoint_install_user_roles() {
  // Enable default permissions for system roles.
  $filtered_html_permission = filter_permission_name(filter_format_load('filtered_html'));
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content', $filtered_html_permission));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content', $filtered_html_permission));
  
  // Create default roles and assign permissions
  _create_administrator_role();
  _create_astonish_role();
}

/**
 * Create administrator role
 * 
 * Saves role "administrator" and then grants 
 * basic permissions.
 */
function _create_administrator_role() {
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 2;
  user_role_save($admin_role);
  
  user_role_grant_permissions($admin_role->rid, array(
     'access contextual links',
     'access dashboard',
     'customize shortcut links',
     'switch shortcut sets',
     'access administration pages',
     'access site in maintenance mode',
     'view the administration theme',
     'access site reports',
     'administer users',
     'access user profiles',
     'change own username',
  ));

}

/**
 * Create astonish role
 * 
 * Creates the "god" role astonish, assigns all permissions to it, and then
 * assigns uid 1 to the astonish role.
 */
function _create_astonish_role() {
  $astonish_role = new stdClass();
  $astonish_role->name = 'astonish';
  $astonish_role->weight = 3;
  user_role_save($astonish_role);
  user_role_grant_permissions($astonish_role->rid, array_keys(module_invoke_all('permission')));
  
  // Set this as the administrator role.
  variable_set('user_admin_role', $astonish_role->rid);
}