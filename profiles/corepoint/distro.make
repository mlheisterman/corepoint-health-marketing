api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7.12"

; Make system directories configurable to allow tests in profiles/[name]/modules to be run.
; http://drupal.org/node/911354
projects[drupal][patch][911354] = http://drupal.org/files/issues/911354.43.patch

; Missing drupal_alter() for text formats and filters
; http://drupal.org/node/903730
projects[drupal][patch][903730] = http://drupal.org/files/issues/drupal.filter-alter.92.patch

; Use vocabulary machine name for permissions
; http://drupal.org/node/995156
projects[drupal][patch][995156] = http://drupal.org/files/issues/995156-5_portable_taxonomy_permissions.patch

projects[buildkit][type] = profile
projects[buildkit][download][type] = git
projects[buildkit][download][url] = http://git.drupal.org/project/buildkit.git
projects[buildkit][download][branch] = 7.x-2.x

projects[corepoint][type] = "profile"
projects[corepoint][download][type] = "git"
projects[corepoint][download][url] = "gitolite@repo.astonishdesigns.com:corepoint.git"
projects[corepoint][download][working-copy] = "true"
